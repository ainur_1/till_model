import numpy as np
import numba as nb
from scipy import integrate


def expand_period_state(S0,k):
    x_size = S0.shape[0]
    y_size = S0.shape[1]
    S = S0.reshape(x_size,y_size,3)
    S_new = np.zeros([x_size+2*k,y_size+2*k,3])
    S_new[k:-k,k:-k] = S
    
    S_new[:k,k:-k] = S[-k:]
    S_new[x_size+k:,k:-k] = S[:k]
    
    S = S_new[:,k:k+y_size]
    S_new[:,:k] = S[:,-k:]
    S_new[:,y_size+k:] = S[:,:k]
    
    return S_new


# Находит центр скирмиона
def skyrmion_center(S0):
    
    x_size= S0.shape[0]
    y_size = S0.shape[1]
    S = S0.reshape(x_size,y_size,3).copy()
    
    centers=np.array([0.0,0.0])
    weights = -S[:,:,2]
    weights[weights<0]=0
    M = d2_Simpson(weights,1)
    x=np.arange(x_size)
    y = np.arange(y_size)

    centers[0] = d2_Simpson(x[:,None]*weights,1)/M
    centers[1] = d2_Simpson(y[None,:]*weights,1)/M
    
    return centers


# Находит радиус скермиона
def skyrmion_rad(S):
    
    x_size = S.shape[0]
    y_size = S.shape[1]
    center = skyrmion_center(S)
    
    S_n = S.reshape(x_size,y_size,3).copy()
    S_z = S_n[:,:,2]
    S_z = S_z - 1
    X = np.zeros([x_size,y_size,2])
    X[:,:,1]=np.arange(y_size)[None,:]
    X[:,:,0]=np.arange(x_size)[:,None]

    X[:,:,0]=X[:,:,0]-center[0]
    X[:,:,1]=X[:,:,1]-center[1]
    X_2 = X[:,:,0]**2 + X[:,:,1]**2
    L = d2_Simpson(X_2*S_z,1)
    M = d2_Simpson(S_z,1)
    R = (2*L/M)**0.5
    return R


# градиент от S0 по 4м точкам 

def grad(S,a):
    x_size = S.shape[0]
    y_size = S.shape[1]

    S_exp = expand_period_state(S,2)
    
    grad_x=np.zeros([x_size,y_size,3])
    grad_x = ((1/12)*S_exp[0:-4,2:-2] - (2/3)*S_exp[1:-3,2:-2] + (2/3)*S_exp[3:-1,2:-2] - (1/12)*S_exp[4:,2:-2])/a
    
    grad_y=np.zeros([x_size,y_size,3])
    grad_y = ((1/12)*S_exp[2:-2,0:-4] - (2/3)*S_exp[2:-2,1:-3] + (2/3)*S_exp[2:-2,3:-1] - (1/12)*S_exp[2:-2,4:])/a
    
    return grad_x, grad_y

# радиальная мода, X - массив с векторами (R - r), где R - координата центра скермиона, r - координата узла решетки
def R_mod(grad_x,grad_y,X):
    
    res = np.zeros([grad_x.shape[0],grad_x.shape[1],grad_x.shape[2]])
    res[:,:,0] = grad_x[:,:,0]*X[:,:,0] + grad_y[:,:,0]*X[:,:,1]
    res[:,:,1] = grad_x[:,:,1]*X[:,:,0] + grad_y[:,:,1]*X[:,:,1]
    res[:,:,2] = grad_x[:,:,2]*X[:,:,0] + grad_y[:,:,2]*X[:,:,1]

    
    return res



# двумерный интеграл Симпсона, a - расстояние между узлами интегрирования.

@nb.njit('f8(f8[:],f8)',nogil=True,fastmath=True,parallel=False)
def d1_Simpson(F,h):
    s = F.shape[0]
    res = F[0] + F[-1]
    k = 1
    for i in range(1,s-1):
        if k:
            k = 0
            res += 4*F[i]
        else:
            k = 1
            res += 2*F[i]
            
    return h/3*res


@nb.njit('f8(f8[:,:],f8)',nogil=True,fastmath=True,parallel=False)
def d2_Simpson(F,h):
    s = F.shape[0]
    res = d1_Simpson(F[0],h) + d1_Simpson(F[-1],h)
    k = 1
    for i in range(1,s-1):
        if k:
            k = 0
            res += 4*d1_Simpson(F[i],h)
        else:
            k = 1
            res += 2*d1_Simpson(F[i],h)
            
    return h/3*res


def dEnergy(S,J,D,K,B,a):
    
    x_size = S.shape[0]
    y_size = S.shape[1]
    
    grad_x,grad_y = grad(S,a)
    grad_xx ,grad_xy = grad(grad_x,a)
    grad_yx ,grad_yy = grad(grad_y,a)
    
    D_x = np.array([
        [0,0,0],
        [0,0,1],
        [0,-1,0]
    ])
    
    D_y = np.array([
        [0,0,-1],
        [0,0,0],
        [1,0,0]
    ])
    
    J_en = -J*(grad_xx+grad_yy)
    D_en = -2*D*(Dot(D_x,grad_x)+Dot(D_y,grad_y))
    

    S_z = S[:,:,2]
    KB_en = np.zeros([x_size,y_size,3])
    KB_en[:,:,2] = -(2*K*S_z+B)

    

    res = J_en+D_en+KB_en
    res-=np.sum(res*S,axis=-1)[...,None]*S
    
    return res


def energy_minimizer(S,J,D,K,B,eps,dE,k):

    x_size = S.shape[0]; y_size = S.shape[1]
    max_grad = eps+1
    i = 0
    while(max_grad>eps):
        dEn = dEnergy(S,J,D,K,B,1)
        dE_norm = np.linalg.norm(dEn,axis=-1)
        max_grad = max(dE_norm.reshape(x_size*y_size))

        S-=dE*dEn
        S=S/(np.linalg.norm(S,axis=-1)[:,:,None])
        i+=1
        if i==k:
            print(max_grad)
            i=0

    return S

def count_dS_dt(S,alpha,beta,gamma,v,J,D,K,B,a):
    x_size = S.shape[0]
    y_size = S.shape[1]
    
    grad_x,grad_y = grad(S,a)
    
    H_S = dEnergy(S,J,D,K,B,a)
    v_div = (v[0]*grad_x+v[1]*grad_y)
    res = gamma*np.cross(S,H_S,axis=-1) - alpha*gamma*H_S + (beta-alpha)*np.cross(v_div,S,axis=-1)+(1+alpha*beta)*v_div
    return res/(1+alpha**2)



def Dot(A,b):
    
    if b.ndim == 3:
        
        return np.einsum("ij,nmj->nmi",A,b)

    else:
        return np.einsum("ij,mj->mi",A,b)

