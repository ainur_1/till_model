from .base import skyrmion_center,skyrmion_rad, grad, R_mod, count_dS_dt
from .const import *
import numpy as np
import numba as nb



def count_vel(S,V,Q,W,C,A,alpha,beta,gamma,v,R0,r_p,k_p,sigma,R,L,L_ch):

    M = V+alpha*Q

    En_v = np.array([
        L**2*W[0,0]+L*W[0,1]+W[0,2],
        L**2*W[1,0]+L*W[1,1]+W[1,2],
        L**2*W[2,0]+L*W[2,1]+W[2,2]
    ])
    
    dEn_v = np.zeros([3])
    
    for i in range(r_p.shape[0]):
        dE_p = count_dE_p(S,L,R,r_p[i],R0,sigma,k_p)
        
        if L_ch:
            dEn_v += np.array([dE_p[0],dE_p[1],L**2*dE_p[2]])
        else:
            dEn_v += np.array([dE_p[0],dE_p[1],0])

    v_v = np.array([
        np.sum(v*(beta*C[0]-A[0])),
        np.sum(v*(beta*C[1]-A[1])),
        np.sum(v*(beta*C[2]-A[2]))
    ])


    sum_v = gamma/L*En_v - gamma*dEn_v - v_v

    vel = np.linalg.solve(M,sum_v)
    vel[2] = vel[2]/L**2
    
    return vel


def Till_dynamic(S,J,d,K0,B,K_p,sigma,R,L,r_p,alpha,beta,gamma,v,dt,Time,rep_time,a,L_ch = True):

    x_s = S.shape[0]
    y_s = S.shape[1]
    
    R0 = skyrmion_center(S)
    
    V = count_V(S); Q = count_Q(S); W = count_W(S,a,J,d,K0,B); C = count_C(S); A = count_A(S)
    
    iter_k = int(Time//dt)
    iter_k_w = int(Time//rep_time)
    res = np.zeros((iter_k_w+10,4))
    time=0.0
    res[0,0]=time; res[0,1:3]=R; res[0,3]=L 
    dif = np.empty(3)
     
    k = 0
    k_w = 0
    for i in range(1,iter_k+1):
        
        time+=dt
        vel = count_vel(S,V,Q,W,C,A,alpha,beta,gamma,v,R0,r_p,K_p,sigma,R,L,L_ch)
        R = R + dt*vel[0:2]
        L = L + dt*vel[2]
        
        k+=1
        
        if k == int(rep_time/dt):
            k_w +=1
            res[k_w,0]=time; res[k_w,1:3]=R; res[k_w,3]=L
            
            k=0
            print('Time = ')
            print(time)
            print('R = ')
            print(R)
            print('L = ')
            print(L)
    
    return res

