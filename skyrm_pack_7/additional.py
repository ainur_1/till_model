import magnes
from .base import energy_minimizer
import numpy as np
from magnes.graphics import plot_field

def make_skyrm(a,L,x_s,y_s,pos):
    
    
    a1 = a*L; x_size = x_s; y_size = y_s; J = 1.4; d = 0.4*a1; K0 = 0.23*a1**2; B = -0.02*a1**2; rad = 8/a1
    size = [x_size,y_size]
    primitives = [(1., 0., 0.), (0., 1., 0.)]
    representatives = [(0., 0., 0.)]
    bc = [magnes.BC.PERIODIC,magnes.BC.PERIODIC] #[magnes.BC.FREE,magnes.BC.FREE]
    origin = magnes.magnetic.Vertex(cell = [0,0])
    
    
    plot = True
    maxtime = 600 #seconds or None
    alpha1 = 0.22 # 0.22 works well for 2D systems, for 3D you should better take 0.01 or so
    precision = 1e-5 # 1e-4 - rough, 1e-5 good, less may be usefull only with double precision
    catcher = magnes.EveryNthCatcher(100) #100 - illustrative, 5000 - fast
    
    reporters = [magnes.TextStateReporter()]
    if plot:
        reporters.append(magnes.graphics.GraphStateReporter())
        reporters.append(magnes.graphics.VectorStateReporter())
    
    
    system = magnes.System(primitives, representatives, size, bc)
    system.add(magnes.magnetic.Exchange(origin,magnes.magnetic.Vertex([1,0]),J,[d,0.,0.]))
    system.add(magnes.magnetic.Exchange(origin,magnes.magnetic.Vertex([0,1]),J,[0.,d,0.]))
    system.set_external_field([0.0,0.0,B])
    K = np.ones([x_size,y_size,1])*K0
    system.add(magnes.Anisotropy(K))

    minimizer = magnes.StateGDwM(system, reference = None, stepsize = alpha1, maxiter = None, maxtime = maxtime, precision = precision, reporter = magnes.MultiReporter(reporters), catcher = catcher, speedup = 5) #speedup = 5 safety, >10 - fast


    state=system.field3D().constant_vector([0,0,1])

    state.append_skyrmion(center=[pos[0],pos[1],0.], radius=rad, winding=[-1,0,0])
    state.satisfy_constrains()
    minimizer.optimize(state)
    S = state.download_copy()
    S = S.reshape([x_size,y_size,3])
    
    S = energy_minimizer(S,J,d,K0,B,10**(-4),0.01,1000)
    
    return S