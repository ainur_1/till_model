from .base import d2_Simpson, skyrmion_center, grad, R_mod, dEnergy
import numpy as np
import numba as nb

def count_V(S):
    x_s = S.shape[0]
    y_s = S.shape[1]
    R0 = skyrmion_center(S)
    
    grad_x,grad_y = grad(S,1)
    
    X = np.zeros([x_s,y_s,2])
    X[:,:,0] = R0[0] - np.arange(x_s)[:,None]
    X[:,:,1] = R0[1] - np.arange(y_s)[None,:]
    
    R_mod = grad_x*X[:,:,0][:,:,None]+grad_y*X[:,:,1][:,:,None]
    
    V = np.zeros([3,3])
    
    V[0,0] = d2_Simpson(np.sum(np.cross(S,grad_x,axis=-1)*grad_x,axis=-1),1)
    V[0,1] = d2_Simpson(np.sum(np.cross(S,grad_y,axis=-1)*grad_x,axis=-1),1)
    V[0,2] = d2_Simpson(np.sum(np.cross(S,R_mod,axis=-1)*grad_x,axis=-1),1)
    
    V[1,0] = d2_Simpson(np.sum(np.cross(S,grad_x,axis=-1)*grad_y,axis=-1),1)
    V[1,1] = d2_Simpson(np.sum(np.cross(S,grad_y,axis=-1)*grad_y,axis=-1),1)
    V[1,2] = d2_Simpson(np.sum(np.cross(S,R_mod,axis=-1)*grad_y,axis=-1),1)
    
    V[2,0] = d2_Simpson(np.sum(np.cross(S,grad_x,axis=-1)*R_mod,axis=-1),1)
    V[2,1] = d2_Simpson(np.sum(np.cross(S,grad_y,axis=-1)*R_mod,axis=-1),1)
    V[2,2] = d2_Simpson(np.sum(np.cross(S,R_mod,axis=-1)*R_mod,axis=-1),1)
    
    return V

def count_Q(S):
    
    x_s = S.shape[0]
    y_s = S.shape[1]
    R0 = skyrmion_center(S)
    
    grad_x,grad_y = grad(S,1)
    
    X = np.zeros([x_s,y_s,2])
    X[:,:,0] = R0[0] - np.arange(x_s)[:,None]
    X[:,:,1] = R0[1] - np.arange(y_s)[None,:]
    
    R_mod = grad_x*X[:,:,0][:,:,None]+grad_y*X[:,:,1][:,:,None]
    
    Q = np.zeros([3,3])
    
    Q[0,0] = d2_Simpson(np.sum(grad_x*grad_x,axis=-1),1)
    Q[0,1] = d2_Simpson(np.sum(grad_y*grad_x,axis=-1),1)
    Q[0,2] = d2_Simpson(np.sum(R_mod*grad_x,axis=-1),1)
    
    Q[1,0] = d2_Simpson(np.sum(grad_x*grad_y,axis=-1),1)
    Q[1,1] = d2_Simpson(np.sum(grad_y*grad_y,axis=-1),1)
    Q[1,2] = d2_Simpson(np.sum(R_mod*grad_y,axis=-1),1)
    
    Q[2,0] = d2_Simpson(np.sum(grad_x*R_mod,axis=-1),1)
    Q[2,1] = d2_Simpson(np.sum(grad_y*R_mod,axis=-1),1)
    Q[2,2] = d2_Simpson(np.sum(R_mod*R_mod,axis=-1),1)
    
    return Q

def count_A(S):
    
    x_s = S.shape[0]
    y_s = S.shape[1]
    R0 = skyrmion_center(S)
    
    grad_x,grad_y = grad(S,1)
    
    X = np.zeros([x_s,y_s,2])
    X[:,:,0] = R0[0] - np.arange(x_s)[:,None]
    X[:,:,1] = R0[1] - np.arange(y_s)[None,:]
    
    R_mod = grad_x*X[:,:,0][:,:,None]+grad_y*X[:,:,1][:,:,None]
    
    A = np.zeros([3,2])
    
    A[0,0] =  d2_Simpson(np.sum(np.cross(grad_x,S,axis=-1)*grad_x,axis=-1),1)
    A[0,1] =  d2_Simpson(np.sum(np.cross(grad_y,S,axis=-1)*grad_x,axis=-1),1)
    
    A[1,0] =  d2_Simpson(np.sum(np.cross(grad_x,S,axis=-1)*grad_y,axis=-1),1)
    A[1,1] =  d2_Simpson(np.sum(np.cross(grad_y,S,axis=-1)*grad_y,axis=-1),1)
    
    A[2,0] =  d2_Simpson(np.sum(np.cross(grad_x,S,axis=-1)*R_mod,axis=-1),1)
    A[2,1] =  d2_Simpson(np.sum(np.cross(grad_y,S,axis=-1)*R_mod,axis=-1),1)    
    
    return A
    


def count_C(S):
    
    x_s = S.shape[0]
    y_s = S.shape[1]
    R0 = skyrmion_center(S)
    
    grad_x,grad_y = grad(S,1)
    
    X = np.zeros([x_s,y_s,2])
    X[:,:,0] = R0[0] - np.arange(x_s)[:,None]
    X[:,:,1] = R0[1] - np.arange(y_s)[None,:]
    
    R_mod = grad_x*X[:,:,0][:,:,None]+grad_y*X[:,:,1][:,:,None]
    
    C = np.zeros([3,2])
    
    C[0,0] =  d2_Simpson(np.sum(grad_x*grad_x,axis=-1),1)
    C[0,1] =  d2_Simpson(np.sum(grad_y*grad_x,axis=-1),1)
    
    C[1,0] =  d2_Simpson(np.sum(grad_x*grad_y,axis=-1),1)
    C[1,1] =  d2_Simpson(np.sum(grad_y*grad_y,axis=-1),1)
    
    C[2,0] =  d2_Simpson(np.sum(grad_x*R_mod,axis=-1),1)
    C[2,1] =  d2_Simpson(np.sum(grad_y*R_mod,axis=-1),1)    
    
    return C


def count_W(S,a,J,D,K,B):
    x_s = S.shape[0]
    y_s = S.shape[1]
    
    R0 = skyrmion_center(S)
    
    grad_x,grad_y = grad(S,a)
    
    X = np.zeros([x_s,y_s,2])
    X[:,:,0] = R0[0] - np.arange(x_s)[:,None]
    X[:,:,1] = R0[1] - np.arange(y_s)[None,:]
    
    R_mod = grad_x*X[:,:,0][:,:,None]+grad_y*X[:,:,1][:,:,None]
        
    W = np.zeros([3,3])
    dEn_J = dEnergy(S,J,0,0,0,a); dEn_D = dEnergy(S,0,D,0,0,a); dEn_K = dEnergy(S,0,0,K,B,a)
    
    W[0,0] = d2_Simpson(np.sum(dEn_J*grad_x,axis=-1),1)
    W[0,1] = d2_Simpson(np.sum(dEn_D*grad_x,axis=-1),1)
    W[0,2] = d2_Simpson(np.sum(dEn_K*grad_x,axis=-1),1)
    
    W[1,0] = d2_Simpson(np.sum(dEn_J*grad_y,axis=-1),1)
    W[1,1] = d2_Simpson(np.sum(dEn_D*grad_y,axis=-1),1)
    W[1,2] = d2_Simpson(np.sum(dEn_K*grad_y,axis=-1),1)
    
    W[2,0] = d2_Simpson(np.sum(dEn_J*R_mod,axis=-1),1)
    W[2,1] = d2_Simpson(np.sum(dEn_D*R_mod,axis=-1),1)
    W[2,2] = d2_Simpson(np.sum(dEn_K*R_mod,axis=-1),1)
    
    return W



def count_dE_p(S,L,R,r_p,R0,sigma,k_p):
    
    if ((R[0]-r_p[0])**2 + (R[1]-r_p[1])**2)**0.5 >20:
        return np.array([0,0,0])
    
    x_s = S.shape[0]
    y_s = S.shape[1]
    
    r_0 = R0 - L*(R-r_p)
    
    r_l = (r_0 - np.array([8,8])).astype(int)
    r_l[0] = max(0,r_l[0]); r_l[1] = max(0,r_l[1])
    
    r_r = (r_0 + np.array([9,9])).astype(int)
    r_r[0] = min(x_s,r_r[0]); r_r[1] = min(y_s,r_r[1])
    
    X = np.zeros([x_s,y_s,2])
    X[:,:,0] = (np.arange(x_s)[:,None]-R0[0])/L + R[0] - r_p[0]
    X[:,:,1] = (np.arange(y_s)[None,:]-R0[1])/L + R[1] - r_p[1]
    
    X = X[r_l[0]:r_r[0],r_l[1]:r_r[1]]
    X_2 = X[:,:,0]**2 + X[:,:,1]**2
    
    K_p = k_p*np.exp(-X_2/sigma)
    S_z_2 = (S[:,:,2]**2)[r_l[0]:r_r[0],r_l[1]:r_r[1]]
    
    dE_p_x = 2/(L**2*sigma)*d2_Simpson(K_p*S_z_2*X[:,:,0],1)
    dE_p_y = 2/(L**2*sigma)*d2_Simpson(K_p*S_z_2*X[:,:,1],1)
    
    X_h = np.zeros([x_s,y_s,2])
    X_h[:,:,0] = -(np.arange(x_s)[:,None]-R0[0])/L**2
    X_h[:,:,1] = -(np.arange(y_s)[None,:]-R0[1])/L**2
    X_h = X_h[r_l[0]:r_r[0],r_l[1]:r_r[1]]
    
    dE_p_L = 2/(L**2*sigma)*d2_Simpson(K_p*S_z_2*(X[:,:,0]*X_h[:,:,0]+X[:,:,1]*X_h[:,:,1]),1)
    dE_p_L += 2/L**3*d2_Simpson(K_p*S_z_2,1)
    
    return np.array([dE_p_x,dE_p_y,dE_p_L])






